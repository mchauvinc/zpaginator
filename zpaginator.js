var Zest = Zest || {};
Zest.Paginator = Backbone.View.extend({
    initialize:function(box,load){
        this.box = $(box);
        this.url = this.readUrl();
        this.getDefaultPaginationValues();
        this.addPagingEvents();
        this.addOrderingEvents();
        this.on("dataLoaded", this.onDataLoaded);
        this.on("load", this.loadData);
        if(load){
            this.trigger("load");
        }
    },
    getDefaultPaginationValues:function(){
        this.page = this.box.attr("data-default-page");
        this.order = this.box.attr("data-default-order");
        this.orderDir = this.box.attr("data-default-order-dir");
    },
    addPagingEvents:function(){
        this.box.delegate(this.getPagingElements(),"click",_.bind(this.loadData, this));
    },
    addOrderingEvents:function(){
        this.box.delegate(this.getOrderingElements(),"click",_.bind(this.loadData, this));
    },
    getPagingElements: function(){
        return ".paging-element";
    },
    getOrderingElements:function(){
        return ".ordering-element";
    },
    getEntries: function(){
        return this.box.find("tbody tr");
    },
    loadData:function(event){
        if(event){
            this.setPage(event);
            this.setOrder(event);
            this.setOrderDir(event);
        }
        this.trigger("beforeLoad", this);
        var that=this;
        return $.getJSON(this.url,_.extend(this.getPaginationDetails(),this.getExtraData())).done(function(response){
            var success=that.isSuccessResponse(response);
            if(success){
                that.trigger("successData", response);
            } else {
                that.trigger("failureData", response);
            }
            that.trigger("dataLoaded", response, success);
        });
    },
    getExtraData: function() {
        return {};
    },
    isSuccessResponse: function(response){
        return response.success;
    },
    setPage: function(event){
        this.page = $(event.target).attr("data-page");
    },
    setOrder: function(event){
        this.order = $(event.target).attr("data-order");
    },
    setOrderDir: function(event){
        this.orderDir = $(event.target).attr("data-order-dir");
    },
    getPaginationDetails: function(){
        return {
            page: this.page,
            order:this.order,
            orderDir: this.orderDir
        }
    },
    readUrl:function(){
        return this.box.attr("data-url");
    },
    onDataLoaded: function(response, success){
        this.renderResponse(response, success);
    },
    renderResponse:function(response, success){
        if(success){
            this.box.html(this.responseToHtml(response));
        }
        this.trigger("dataRendered");
    },
    responseToHtml:function(response){
        return response.html;
    },
    getElement:function(){
        return this.box;
    },
    delegate: function(selector, eventType, callback){
        this.getElement().delegate(selector, eventType, callback);
        return this;
    },
    hasEntries: function () {
        return this.getEntries().length > 0;
    }
});


// Extend to include filters
Zest.FilteredPaginator = Zest.Paginator.extend({
    // Filters must be a dictionary of name => jquery element
    initialize: function(box, load, filters) {
        Zest.Paginator.prototype.initialize.apply(this, [box, load]);
        this.filters = filters;
    },
    
    getExtraData: function() {
        var data = {};
        _(this.filters).each(function(item, k){
            data[k] = item.val()
        });
        return data;
    }
})
